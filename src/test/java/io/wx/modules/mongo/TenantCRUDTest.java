/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo;

import io.vertx.core.Handler;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.wx.core3.common.WXTools;
import io.wx.modules.mongo.app.ctr.TenantCampaignCRUDController;
import io.wx.modules.mongo.app.entities.TenantCampaign;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Random;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 */
@RunWith(VertxUnitRunner.class)
public class TenantCRUDTest {

  public static MongoApplication myApp;
  public static String RESOURCE_URL = "/test/api/tenant/campaign/";

  @BeforeClass
  public static void setUpApplication() throws Exception {
    myApp = new MongoApplication();
    myApp.start();
    new TestSuite(myApp).init();
  }

  @AfterClass
  public static void setDownApplication() throws Exception {
    myApp.getCore().getVertx().close();
    myApp = null;
  }
  
  @Test
  public void getEntityWithBadTenant(TestContext context) {
    String expected = "";
    String entityID = "cp1";  //this entity has another tenant

    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/" + entityID);
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(404, resp.statusCode());
      async1.complete();
    });
    req.end();
  }
  
  @Test
  public void testCreateEntity(TestContext context) {
    TenantCampaign campaign = new TenantCampaign();
    campaign.setTitle("new campaign");
    campaign.setId("newEntityID");

    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/" + campaign.getId());
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(404, resp.statusCode());
      async1.complete();
    });
    req.end();
    
    async1.awaitSuccess();
    
    Async async2 = context.async();
    req = client.post(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL);
    req.handler(response -> {
      context.assertEquals(200, response.statusCode());
      async2.complete();
    });
    req.putHeader("content-type", "application/json");
    req.putHeader("content-length", "" + WXTools.toJSON(campaign).length());
    req.write(WXTools.toJSON(campaign));
    req.end();
    
    async2.awaitSuccess();
    
    Async async3 = context.async();
    client = myApp.getCore().getVertx().createHttpClient();
    req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/" + campaign.getId());
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());
      resp.bodyHandler(body -> {
        try {
          TenantCampaign entity = WXTools.mapper.readValue(body.toString(), TenantCampaign.class);
          context.assertNotNull(entity);
          context.assertEquals(campaign.getId(), entity.getId());
          context.assertEquals(entity.getTenant(), TenantCampaignCRUDController.TENANT);
        } catch (IOException ex) {
          context.fail("Error getting entity");
        }
        finally{
          async3.complete();
        }
      });
    });
    req.end();
    
  }
  
  @Test
  public void testUpdateEntity(TestContext context) {
    String expected = "cp2 updated";
    TenantCampaign campaign = new TenantCampaign();
    campaign.setId("cp2");
    campaign.setTenant("We try to change the tenant");
    
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/" + campaign.getId());
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());
      resp.bodyHandler(body -> {
        try {
          TenantCampaign entity = WXTools.mapper.readValue(body.toString(), TenantCampaign.class);
          context.assertNotNull(entity);
          context.assertNotEquals(entity.getTitle(), expected);
          context.assertEquals(entity.getTenant(), TenantCampaignCRUDController.TENANT);
        } catch (IOException ex) {
          context.fail("Error getting entity");
        }
        finally{
          async1.complete();
        }
      });
    });
    req.end();
    
    async1.awaitSuccess();
    
    campaign.setTitle(expected);
    Async async2 = context.async();
    req = client.put(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL);
    req.handler(response -> {
      context.assertEquals(200, response.statusCode());
      async2.complete();
    });
    req.putHeader("content-type", "application/json");
    req.putHeader("content-length", "" + WXTools.toJSON(campaign).length());
    req.write(WXTools.toJSON(campaign));
    req.end();
    
    async2.awaitSuccess();
    
    Async async3 = context.async();
    client = myApp.getCore().getVertx().createHttpClient();
    req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/" + campaign.getId());
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());
      resp.bodyHandler(body -> {
        try {
          TenantCampaign entity = WXTools.mapper.readValue(body.toString(), TenantCampaign.class);
          context.assertNotNull(entity);
          context.assertEquals(entity.getTitle(), expected);
          context.assertEquals(entity.getTenant(), TenantCampaignCRUDController.TENANT);
        } catch (IOException ex) {
          context.fail("Error getting entity");
        }
        finally{
          async3.complete();
        }
      });
    });
    req.end();
    
  }
  
  @Test
  public void testBadRequest(TestContext context) {
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    
    String data = "{_id:XXX, fieldA: XXX,,,,}";
    HttpClientRequest request = client.post(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL, new Handler<HttpClientResponse>() {
      @Override
      public void handle(HttpClientResponse resp) {
        context.assertEquals(400, resp.statusCode());
        async1.complete();
      }
    });
    request.putHeader("Content-Length", Integer.toString(data.length()));
    request.end(data);
  }
  
  @Test
  public void testGetAllEntities(TestContext context) {
    String expected = "";
    
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL);
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());
      resp.bodyHandler(body -> {
        try {
          TenantCampaign[] entities = WXTools.mapper.readValue(body.toString(), TenantCampaign[].class);
          context.assertTrue(entities.length > 0);
        } catch (IOException ex) {
          context.fail("Error getting entities");
        }
        finally{
          async1.complete();
        }
      });
    });
    req.end();
  }
  
  @Test
  public void testGetEntity(TestContext context) {
    String expected = "";
    String entityID = "cp3";
    
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/" + entityID);
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());
      resp.bodyHandler(body -> {
        try {
          TenantCampaign entity = WXTools.mapper.readValue(body.toString(), TenantCampaign.class);
          context.assertNotNull(entity);
          context.assertEquals(entityID, entity.getId());
          context.assertEquals(entity.getTenant(), TenantCampaignCRUDController.TENANT);
        } catch (IOException ex) {
          context.fail("Error getting entity");
        }
        finally{
          async1.complete();
        }
      });
    });
    req.end();
  }
  
  @Test
  public void testDeleteEntity(TestContext context) {
    String expected = "";
    String entityID = "cp2";
    
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/" + entityID);
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());
      resp.bodyHandler(body -> {
        try {
          TenantCampaign entity = WXTools.mapper.readValue(body.toString(), TenantCampaign.class);
          context.assertNotNull(entity);
          context.assertEquals(entityID, entity.getId());
          context.assertEquals(entity.getTenant(), TenantCampaignCRUDController.TENANT);
        } catch (IOException ex) {
          context.fail("Error getting entity");
        }
        finally{
          async1.complete();
        }
      });
    });
    req.end();
    
    async1.awaitSuccess();
    
    Async async2 = context.async();
    req = client.delete(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL+ "/" + entityID);
    req.handler(response -> {
      context.assertEquals(200, response.statusCode());
      async2.complete();
    });
    req.end();
    
    async2.awaitSuccess();
    
    Async async3 = context.async();
    client = myApp.getCore().getVertx().createHttpClient();
    req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/" + entityID);
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(404, resp.statusCode());
      async3.complete();
    });
    req.end();
    
  }
  
  @Test
  public void testDeleteEntityThatDoesNotExist(TestContext context) {
    Random randomGenerator = new Random();
    Integer expected = 404;
    String entityID = "xxx" + randomGenerator.nextInt(100);
    
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/" + entityID);
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(expected, resp.statusCode());
      async1.complete();
    });
    req.end();
    
    async1.awaitSuccess();
    
    Async async2 = context.async();
    req = client.delete(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL+ "/" + entityID);
    req.handler(response -> {
      context.assertEquals(expected, response.statusCode());
      async2.complete();
    });
    req.end();
  }
  
  @Test
  public void testGetEntityByTitle(TestContext context) throws UnsupportedEncodingException {
    String expected = "Title cp3";
    
    String title = expected;
    String query = URLEncoder.encode(title, "UTF-8");
    
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/?title=" + query);
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());
      resp.bodyHandler(body -> {
        try {
          TenantCampaign[] entities = WXTools.mapper.readValue(body.toString(), TenantCampaign[].class);
          context.assertNotNull(entities);
          context.assertEquals(expected, entities[0].getTitle());
          context.assertEquals(entities[0].getTenant(), TenantCampaignCRUDController.TENANT);
        } catch (IOException ex) {
          context.fail("Error getting entity");
        }
        finally{
          async1.complete();
        }
      });
    });
    req.end();
  }

}
