package io.wx.modules.mongo;

import io.wx.core3.config.SimpleConfig;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 */
public class TestPerformance extends MongoApp {

    private TestPerformance() throws Exception  {
        super(MongoApp.DEFAULT_POOL_NAME, new SimpleConfig().setPort(8081).setSSL(true));
    }

    public static void main(String[] args) throws Exception  {
        new TestPerformance().start().idle();
    }

    @Override
    public String getApplicationPath() {
        return "io.wx.modules.mongo.app";
    }

}
