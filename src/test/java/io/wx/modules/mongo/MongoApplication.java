/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo;

/**
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 */
public class MongoApplication extends MongoApp {

  public static void main(String[] args) throws Exception {
    MongoApplication instance = new MongoApplication();
    instance.start().idle();
  }

  public MongoApplication() throws Exception {
    // Customize the application method 1
    //super(new MongoConfigurator().setMongoTimeout(1).setMongoAppTerminate(false));
    
    // Customize the application method 2
    //super(new MongoConfigurator().setMongoTimeout(5));
    super();
    
  }

  @Override
  public String getApplicationPath() {
    return "io.wx.modules.mongo.app";
  }

}
