package io.wx.modules.mongo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.wx.core3.common.WXTools;
import static io.wx.modules.mongo.CRUDTest.myApp;
import io.wx.modules.mongo.entity.BaseEntity;
import io.wx.modules.mongo.app.ctr.TenantCampaignCRUDController;
import io.wx.modules.mongo.app.entities.Campaign;
import io.wx.modules.mongo.app.entities.Formular;
import io.wx.modules.mongo.app.entities.Layout;
import io.wx.modules.mongo.app.entities.Page;
import io.wx.modules.mongo.app.entities.TenantCampaign;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author moritz
 */
public class TestSuite {

  public static final Logger logger = LogManager.getLogger(TestSuite.class);

  MongoApplication myApp;
  private MongoClient mongoClient;

  public TestSuite(MongoApplication myApp) {
    this.myApp = myApp;
  }
  
  
  public void init() throws Exception {    
    mongoClient = MongoClient.createShared(myApp.getCore().getVertx(), getPersistorCfg());
    prepareMongoData();
    logger.info("Ready for testing :)");
  }

  private void prepareMongoData() throws InterruptedException, IOException {
    logger.info("Removing collections ...");
    removeCollections(new String[]{
      "campaigns",
      "campaigns_tenant",
      "formulars_tenant",
      "pages",
      "layouts",
      "formulars",
      "downloads"
    });
    logger.info("Inserting entities for testing");
    createEntitiesForTest();
  }

  private JsonObject getPersistorCfg() {
    JsonObject persistorCfg = new JsonObject();
    String mongoURL = "mongodb://localhost:27017";
    persistorCfg.put("connection_string", mongoURL);
    persistorCfg.put("db_name", "MongoPersistorTest");
    return persistorCfg;
  }


  private void removeCollections(String[] collections) throws InterruptedException {    
    for (String collection : collections) {
      removeCollection(collection);
    }    
  }
  
  private void removeCollection(String collection) throws InterruptedException {    
    CountDownLatch latch = new CountDownLatch(1);
    mongoClient.dropCollection(collection, res -> latch.countDown());
    latch.await();    
  }

  private void createEntitiesForTest() throws IOException, InterruptedException {
    List<BaseEntity> entities = new ArrayList();
    Campaign campaign = new Campaign();
    campaign.setId("cp1");
    campaign.setTitle("Title cp1");
    campaign.setCreated(new Date());

    entities.add(campaign);

    campaign = new Campaign();
    campaign.setId("cp2");
    campaign.setTitle("Title cp2");
    campaign.setCreated(new Date());

    entities.add(campaign);

    campaign = new Campaign();
    campaign.setId("cp3");
    campaign.setTitle("Title cp3");
    campaign.setCreated(new Date());

    entities.add(campaign);

    TenantCampaign tenantCampaign = new TenantCampaign();
    tenantCampaign.setId("cp1");
    tenantCampaign.setTenant("BadTenant");
    tenantCampaign.setTitle("Title cp1");
    tenantCampaign.setCreated(new Date());

    entities.add(tenantCampaign);

    tenantCampaign = new TenantCampaign();
    tenantCampaign.setId("cp2");
    tenantCampaign.setTenant(TenantCampaignCRUDController.TENANT);
    tenantCampaign.setTitle("Title cp2");
    tenantCampaign.setCreated(new Date());

    entities.add(tenantCampaign);

    tenantCampaign = new TenantCampaign();
    tenantCampaign.setId("cp3");
    tenantCampaign.setTenant(TenantCampaignCRUDController.TENANT);
    tenantCampaign.setTitle("Title cp3");
    tenantCampaign.setCreated(new Date());

    entities.add(tenantCampaign);

    Campaign campaignRelationship = new Campaign();
    campaignRelationship.setId("cpR1");
    campaignRelationship.setTitle("Title cpR1");
    campaignRelationship.setCreated(new Date());

    entities.add(campaignRelationship);

    Page page = new Page();
    page.setId("page1");
    page.setTitle("Title page1");
    page.setCampaignID("cpR1");
    page.setCreated(new Date());

    entities.add(page);

    page = new Page();
    page.setId("page2");
    page.setTitle("Title page1");
    page.setCampaignID("cpR1");
    page.setCreated(new Date());

    entities.add(page);

    page = new Page();
    page.setId("page3");
    page.setTitle("Title page1");
    page.setCampaignID("cpR2");
    page.setCreated(new Date());

    entities.add(page);

    Formular formular = new Formular();
    formular.setId("form1");
    formular.setTitle("Title page1");
    formular.setCampaignID("cpR1");
    formular.setCreated(new Date());

    entities.add(formular);

    formular = new Formular();
    formular.setId("form2");
    formular.setTitle("Title page1");
    formular.setCampaignID("cpR1");
    formular.setCreated(new Date());

    entities.add(formular);

    formular = new Formular();
    formular.setId("form3");
    formular.setTitle("Title page1");
    formular.setCampaignID("cpR2");
    formular.setCreated(new Date());

    entities.add(formular);

    page = new Page();
    page.setId("page4");
    page.setTitle("Title page4");
    page.setCampaignID("cpR2");
    page.setLayoutID("layout1");
    page.setCreated(new Date());

    entities.add(page);

    page = new Page();
    page.setId("page5");
    page.setTitle("Title page5");
    page.setCampaignID("cpR2");
    page.setLayoutID("layout1");
    page.setCreated(new Date());

    entities.add(page);

    page = new Page();
    page.setId("page6");
    page.setTitle("Title page6");
    page.setCampaignID("cpR2");
    page.setLayoutID("layout2");
    page.setCreated(new Date());

    entities.add(page);

    Layout layout = new Layout();
    layout.setId("layout1");
    layout.setCreated(new Date());

    entities.add(layout);

    layout = new Layout();
    layout.setId("layout2");
    layout.setCreated(new Date());

    entities.add(layout);
    
    insertEntities(entities);
  }
  private void insertEntities(List<BaseEntity> entities) throws IOException, InterruptedException {
    for (BaseEntity entity : entities) {     
      insertEntity(entity);
    }
  }
  
  /**
   * We have to wait till all the entites are inserted to start the tests. 
   * To achieve it we use CountDownLatch
  */
  private void insertEntity(BaseEntity entity) throws InterruptedException {
    CountDownLatch latch = new CountDownLatch(1);
    String collectionName = MongoPersistor.getCollectionName(entity.getClass());
    mongoClient.insert(collectionName, new JsonObject(WXTools.toJSON(entity)), res -> latch.countDown());
    latch.await();
  }
  
}
