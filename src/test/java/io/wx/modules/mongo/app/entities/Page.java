/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo.app.entities;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.wx.modules.mongo.entity.CRUDEntity;
import io.wx.modules.mongo.entity.Entity;

/**
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 */
@Entity(collection = "pages")
public class Page extends CRUDEntity {

  private String title;
  private String campaignID;
  private String layoutID;
  private String downloadID;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getCampaignID() {
    return campaignID;
  }

  public void setCampaignID(String campaignID) {
    this.campaignID = campaignID;
  }

  public String getLayoutID() {
    return layoutID;
  }

  public void setLayoutID(String layoutID) {
    this.layoutID = layoutID;
  }

  public String getDownloadID() {
    return downloadID;
  }

  public void setDownloadID(String downloadID) {
    this.downloadID = downloadID;
  }
  
  @Override
  public JsonArray describeIndexes() {
    JsonObject appKey = new JsonObject();
    appKey.put("title", 1);
    JsonObject appIndex = new JsonObject();
    appIndex.put("key", appKey);
    appIndex.put("name", "Page Title");
    JsonArray userIndexes = new JsonArray();
    userIndexes.add(appIndex);
    return userIndexes;
  }
}
