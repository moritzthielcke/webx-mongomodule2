/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo.app.entities;

import io.wx.modules.mongo.entity.CRUDEntity;
import io.wx.modules.mongo.entity.Entity;
import io.wx.modules.mongo.entity.RemoveReferences;
import io.wx.modules.mongo.entity.TargetEntity;

/**
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 */
@Entity(collection = "layouts")
@RemoveReferences(
  {
    @TargetEntity( entity = Page.class, joinColumn = "layoutID") ,
  }
)
public class Layout extends CRUDEntity {
  
}
