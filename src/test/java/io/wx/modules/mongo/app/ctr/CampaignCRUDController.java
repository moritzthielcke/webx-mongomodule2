/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo.app.ctr;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.wx.core3.http.RequestMapping;
import io.wx.core3.http.Resource;
import io.wx.core3.http.URLParam;
import io.wx.modules.mongo.app.entities.Campaign;
import io.wx.modules.mongo.ctr.CRUDController;
import io.wx.modules.mongo.exception.EntityNotFoundException;
import io.wx.modules.mongo.handler.MongoResponseHandler;

/**
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 */
@Resource(path = "/test/api/campaign")
public class CampaignCRUDController extends CRUDController<Campaign>{

  @RequestMapping(path = "/:id/findAndModify/:title", method = HttpMethod.PUT)
  public void findAndModify(@URLParam String id, @URLParam String title) {
    try {
      //try to find the entity
      JsonObject query = getRequestParams();
      query.remove("id");
      query.remove("title");
      query.put("_id", id);
      JsonObject modifications = new JsonObject();
      modifications.put("title", title);
      
      mongoPersistor.findAndModify(query, modifications, new MongoResponseHandler<Campaign>() {
        @Override
        public void finish() {
          try {
            if (success) {
              if (getEntity() != null) {
                handleResponse(getEntity());
              }
              else {
                throw new EntityNotFoundException();
              }
            } else {
              throw new EntityNotFoundException();
            }
          } catch (EntityNotFoundException ex) {
            handleError("cant find and modify entity", ex);
          }
        }
      });
    } catch (Exception ex) {
      handleError("cant find and modify entity", ex);
    }

  }
  
}
