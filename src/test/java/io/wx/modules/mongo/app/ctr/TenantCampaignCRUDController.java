/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo.app.ctr;

import io.wx.core3.http.Resource;
import io.wx.modules.mongo.app.entities.TenantCampaign;
import io.wx.modules.mongo.ctr.TenantCRUDController;

/**
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 */
@Resource(path = "/test/api/tenant/campaign")
public class TenantCampaignCRUDController extends TenantCRUDController<TenantCampaign>{
    public static String TENANT = "tenant_for_tests";
    
    @Override
    public String getTenantID() {
        return TENANT;
    }

}
