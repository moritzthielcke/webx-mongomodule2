/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo.app.entities;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.wx.modules.mongo.entity.CRUDEntity;
import io.wx.modules.mongo.entity.Entity;
import io.wx.modules.mongo.entity.RemoveReferences;
import io.wx.modules.mongo.entity.TargetEntity;

/**
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 */
@Entity(collection = "downloads")
@RemoveReferences(
  {
    @TargetEntity(entity = Page.class, joinColumn = "downloadID"),}
)
public class Download extends CRUDEntity {

  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public JsonArray describeIndexes() {
    JsonObject appKey = new JsonObject();
    appKey.put("name", 1);
    JsonObject appIndex = new JsonObject();
    appIndex.put("key", appKey);
    appIndex.put("name", "Download name");
    JsonArray userIndexes = new JsonArray();
    userIndexes.add(appIndex);
    return userIndexes;
  }
}
