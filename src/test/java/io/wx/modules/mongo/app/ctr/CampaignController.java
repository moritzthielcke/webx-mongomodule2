/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo.app.ctr;

import io.wx.core3.http.Resource;
import io.wx.modules.mongo.app.entities.Campaign;
import io.wx.modules.mongo.ctr.CRUDController;


/**
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 */
@Resource(path = "/campaign")
public class CampaignController extends CRUDController<Campaign>{
}
