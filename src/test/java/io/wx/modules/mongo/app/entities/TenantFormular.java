package io.wx.modules.mongo.app.entities;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.wx.modules.mongo.entity.Entity;
import io.wx.modules.mongo.entity.TenantCRUDEntity;

/**
 *
 * @author Jose Luis Conde <jose.linares@code-mitte.de>
 */
@Entity(collection = "formulars_tenant")
public class TenantFormular extends TenantCRUDEntity{

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public JsonArray describeIndexes(){
        JsonObject appKey = new JsonObject();
        appKey.put("tenant", 1);
        JsonObject appIndex = new JsonObject();                        
        appIndex.put("key", appKey);
        appIndex.put("name", "Formular_Tenant");
        JsonArray userIndexes = new JsonArray();
        userIndexes.add(appIndex); 
        return userIndexes;
    } 

    @Override
    public String toString() {
        return "TenantFormular{" + "id=" + getId() + ", tenant=" + getTenant() + ", title=" + title +'}';
    }
}
