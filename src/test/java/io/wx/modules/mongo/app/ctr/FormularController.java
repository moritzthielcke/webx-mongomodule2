/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo.app.ctr;

import io.wx.core3.http.Resource;
import io.wx.modules.mongo.ctr.CRUDController;
import io.wx.modules.mongo.app.entities.Formular;

/**
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 */
@Resource(path = "/formular")
public class FormularController extends CRUDController<Formular>{
}