package io.wx.modules.mongo.app.entities;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.wx.modules.mongo.entity.CRUDEntity;
import io.wx.modules.mongo.entity.DeleteChild;
import io.wx.modules.mongo.entity.Entity;
import io.wx.modules.mongo.entity.TargetEntity;

/**
 *
 * @author Jose Luis Conde <jose.linares@code-mitte.de>
 */
@Entity(collection = "campaigns")
@DeleteChild(
  {
    @TargetEntity( entity = Page.class, joinColumn = "campaignID") ,
    @TargetEntity( entity = Formular.class, joinColumn = "campaignID") 
  }
)
public class Campaign extends CRUDEntity{

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public JsonArray describeIndexes(){
        JsonObject appKey = new JsonObject();
        appKey.put("title", 1);
        JsonObject appIndex = new JsonObject();                        
        appIndex.put("key", appKey);
        appIndex.put("name", "Campaign Title");
        JsonArray userIndexes = new JsonArray();
        userIndexes.add(appIndex); 
        return userIndexes;
    } 
}
