/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo;

import com.fasterxml.jackson.databind.JavaType;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.mongo.UpdateOptions;
import io.wx.core3.common.WXTools;
import io.wx.modules.mongo.entity.BaseEntity;
import io.wx.modules.mongo.entity.DeleteChild;
import io.wx.modules.mongo.entity.Entity;
import io.wx.modules.mongo.entity.RemoveReferences;
import io.wx.modules.mongo.entity.TargetEntity;
import io.wx.modules.mongo.exception.DataBaseException;
import io.wx.modules.mongo.exception.EntityNotFoundException;
import io.wx.modules.mongo.handler.MongoResponseHandler;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 * @see http://vertx.io/docs/vertx-mongo-client/java/
 */
public class MongoPersistor {

  private static final Logger logger = LogManager.getLogger(MongoPersistor.class);

  @javax.inject.Inject
  private MongoClient mongoClient;

  public <T extends BaseEntity> void findOne(JsonObject query, MongoResponseHandler<T> callback) {
    String collectionName = getCollectionName(callback.getEntityClass());
    mongoClient.findOne(collectionName, query, null, res -> {
      try {
        callback.setSuccess(res.succeeded());
        if (res.succeeded()) {
          JsonObject result = res.result();
          if (result != null) {
            callback.setEntity((T) WXTools.mapper.readValue(result.encode(), callback.getEntityClass()));
          } else {
            callback.setException(new EntityNotFoundException());
          }
        } else {
          callback.setException(new DataBaseException(res.cause().getMessage()));
        }
      } catch (Exception ex) {
        callback.setException(ex);
      }
      callback.finish();
    });
  }

  public <T extends BaseEntity> void find(JsonObject query, MongoResponseHandler<T> callback) {
    String collectionName = getCollectionName(callback.getEntityClass());
    mongoClient.find(collectionName, query, res -> {
      try {
        callback.setSuccess(res.succeeded());
        if (res.succeeded()) {
          List<JsonObject> results = res.result();
          JavaType type = WXTools.mapper.getTypeFactory().constructCollectionType(List.class, callback.getEntityClass());
          callback.setEntities(WXTools.mapper.readValue(results.toString(), type));
        } else {
          callback.setException(new DataBaseException(res.cause().getMessage()));
        }
      } catch (Exception ex) {
        callback.setException(ex);
      }
      callback.finish();
    });
  }

  public <T extends BaseEntity> void remove(JsonObject query, MongoResponseHandler<T> callback) {
    String collectionName = getCollectionName(callback.getEntityClass());
    mongoClient.remove(collectionName, query, res -> {
      try {
        callback.setSuccess(res.succeeded());
        if (res.succeeded()) {
          deleteRelatedEntities(callback.getEntityClass(), query.getString("_id"));
          removeReferences(callback.getEntityClass(), query.getString("_id"));
        } else {
          callback.setException(new DataBaseException(res.cause().getMessage()));
        }
      } catch (Exception ex) {
        callback.setException(ex);
      }
      callback.finish();
    });
  }

  private void deleteRelatedEntities(Class type, String id) {
    DeleteChild deleteAnnotation = (DeleteChild) type.getAnnotation(DeleteChild.class);
    if (deleteAnnotation != null) {
      try {
        TargetEntity[] relatedEntities = deleteAnnotation.value();
        for (TargetEntity relatedEntity : relatedEntities) {
          JsonObject matcher = new JsonObject();
          matcher.put(relatedEntity.joinColumn(), id);
          mongoClient.remove(getCollectionName(relatedEntity.entity()), matcher, res -> {
            if (res.succeeded()) {
              logger.info("Related entities deleted");
            } else {
              logger.info("Error deleting related entities deleted: " + res.cause().getMessage());
            }
          });
        }
      } catch (Exception ex) {
        logger.error("cant delete related entities with id " + id, ex);
      }
    }
  }

  private void removeReferences(Class type, String id) {
    RemoveReferences deleteAnnotation = (RemoveReferences) type.getAnnotation(RemoveReferences.class);
    if (deleteAnnotation != null) {
      try {
        TargetEntity[] relatedEntities = deleteAnnotation.value();
        for (TargetEntity relatedEntity : relatedEntities) {
          JsonObject matcher = new JsonObject();
          matcher.put(relatedEntity.joinColumn(), id);

          JsonObject update = new JsonObject();
          JsonObject modification = new JsonObject();
          String updateColumn = relatedEntity.updateColumn();
          if (updateColumn.equals("")) {
            updateColumn = relatedEntity.joinColumn();
          }
          modification.putNull(updateColumn);
          update.put("$set", modification);
          UpdateOptions options = new UpdateOptions().setMulti(true).setUpsert(false);

          mongoClient.updateWithOptions(getCollectionName(relatedEntity.entity()), matcher, update, options, res -> {
            if (res.succeeded()) {
              logger.info("Related entities deleted");
            } else {
              logger.info("Error deleting related entities deleted: " + res.cause().getMessage());
            }
          });
        }
      } catch (Exception ex) {
        logger.error("cant removeReferencesToEntity with id " + id, ex);
      }
    }
  }

  public <T extends BaseEntity> void save(T entity, MongoResponseHandler<T> callback) {
    String collectionName = getCollectionName(entity.getClass());
    JsonObject jsonEntity = new JsonObject(WXTools.toJSON(entity));
    //To insert new entity the field _id must not exist
    if (jsonEntity.getString("_id") == null) {
      jsonEntity.remove("_id");
    }
    mongoClient.save(collectionName, jsonEntity, res -> {
      try {
        callback.setSuccess(res.succeeded());
        if (res.succeeded()) {
          if (entity.getId() == null) {
            String id = res.result();
            entity.setId(id);
          }
          callback.setEntity(entity);
        } else {
          callback.setException(new DataBaseException(res.cause().getMessage()));
        }
      } catch (Exception ex) {
        callback.setException(ex);
      }
      callback.finish();
    });
  }
  
  
  public <T extends BaseEntity> void findAndModify(JsonObject query, JsonObject modifications, MongoResponseHandler<T> callback) {
    String collectionName = getCollectionName(callback.getEntityClass());
    JsonObject command = new JsonObject();
    JsonObject update = new JsonObject();
    update.put("$set", modifications);
    command.put("findAndModify", collectionName);
    command.put("query", query);
    command.put("update", update);
    command.put("upsert", false);
    command.put("new", true); //returns the modified document rather than the original
    
    mongoClient.runCommand("findAndModify", command, res -> {
      try {
        callback.setSuccess(res.succeeded());
        if (res.succeeded()) {
          JsonObject result = res.result();
          if (result.getJsonObject("value") != null) {
            callback.setEntity((T) WXTools.mapper.readValue(result.getJsonObject("value").encode(), callback.getEntityClass()));
          } else {
            callback.setException(new EntityNotFoundException());
          }
        }
        else {
          callback.setException(new DataBaseException(res.cause().getMessage()));
        }
      } catch (Exception ex) {
        callback.setException(ex);
      }
      callback.finish();
    });
  }

  /**
   * *
   * the collection name is defined by the @link Entity.class annotion, if not
   * its the name of the class
   *
   * @param type
   * @return
   */
  public static String getCollectionName(Class type) {
    Entity entityAnnotation;
    if ((entityAnnotation = (Entity) type.getAnnotation(Entity.class)) == null) {
      return type.getSimpleName().replace(".class", "");
    }
    return entityAnnotation.collection();
  }

}
