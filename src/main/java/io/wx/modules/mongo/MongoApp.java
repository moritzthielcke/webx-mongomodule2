/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo;

import io.vertx.core.json.JsonObject;
import io.wx.core3.config.AppConfig;
import io.wx.core3.config.BasicConfigurator;
import io.wx.core3.config.Configurator;
import io.wx.core3.http.app.AbstractApplication;

/**
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 */
public abstract class MongoApp extends AbstractApplication {

    public static final String DEFAULT_POOL_NAME = "MongoClient." + MongoApp.class;
    private MongoConfigurator mongoConfigurator;

    /**
     * *
     * configures the app with BasicConfigurator.class and
     * MongoConfigurator.class
     *
     * @throws Exception
     */
    public MongoApp() throws Exception {
        this(DEFAULT_POOL_NAME);
    }

    /**
     * *
     * configures the app with the given Configurator-Array and
     * MongoConfigurator.class
     *
     * @param configs
     * @throws Exception
     */
    public MongoApp(Configurator... configs) throws Exception {
        this(new MongoConfigurator(DEFAULT_POOL_NAME), configs);
    }

    /**
     * *
     * configures the app with the given Configurator-Array and
     * MongoConfigurator.class
     *
     * @param poolName
     * @param configs
     * @throws Exception
     */
    public MongoApp(String poolName, Configurator... configs) throws Exception {
        this(new MongoConfigurator(poolName), configs);
    }

    /**
     * *
     * configures the app with BasicConfigurator.class and
     * MongoConfigurator.class
     *
     * @param poolName
     * @throws Exception
     */
    public MongoApp(String poolName) throws Exception {
        this(new MongoConfigurator(poolName), new BasicConfigurator());
    }

    /**
     * *
     * configures the app with BasicConfigurator.class and
     * MongoConfigurator.class
     *
     * @param poolName
     * @param cfg
     * @throws Exception
     */
    public MongoApp(String poolName, AppConfig cfg) throws Exception {
        this(new MongoConfigurator(poolName), new BasicConfigurator(cfg));
    }

    public MongoApp(MongoConfigurator mongoConfigurator) throws Exception {
        this(mongoConfigurator, new BasicConfigurator());
    }

    /**
     * *
     *
     * @param mongoConfigurator the mongo configurator will be executed last and
     * should prepare the application for mongo (@inject persistor)
     * @param configs your other configurator objects ..whatever there for
     * @throws Exception
     */
    public MongoApp(MongoConfigurator mongoConfigurator, Configurator... configs) throws Exception {
        super(configs);
        mongoConfigurator.doConfig(this);
        this.mongoConfigurator = mongoConfigurator;
    }

    public MongoConfigurator getMongoConfigurator() {
        return mongoConfigurator;
    }
}
