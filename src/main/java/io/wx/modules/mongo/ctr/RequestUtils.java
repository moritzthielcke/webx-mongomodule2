/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo.ctr;

import io.vertx.core.json.JsonObject;
import io.wx.core3.http.RequestController;
import io.wx.core3.http.exceptions.HandleResponseException;
import io.wx.modules.mongo.exception.BadRequestException;
import io.wx.modules.mongo.exception.EntityNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 */
public class RequestUtils {

  public static JsonObject getRequestParams(RequestController requestController) {
    JsonObject params = new JsonObject();
    for (String param : requestController.getRequest().params().names()) {
      params.put(param, requestController.getRequest().params().get(param));
    }
    return params;
  }

  public static void handleResponse(RequestController requestController, Object objectToResponse) {
    try {
      //      requestController.getResponseBody().append(objectToResponse);
      requestController.doResponse(objectToResponse).next();
    } catch (HandleResponseException ex) {
      Logger.getLogger(RequestUtils.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  public static void handleError(RequestController requestController, String error, Exception ex) {

    int statusCode;
    if (ex instanceof EntityNotFoundException) {
      statusCode = 404;
    } else if (ex instanceof BadRequestException) {
      statusCode = 400;
    } else {
      statusCode = 500;
    }

    if (statusCode == 500) {
      LogManager.getLogger(RequestUtils.class).error(error, ex);
    }
    requestController.fail(statusCode);
  }
}
