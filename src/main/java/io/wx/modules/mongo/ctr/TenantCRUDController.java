/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo.ctr;

import io.vertx.core.json.JsonObject;
import io.wx.modules.mongo.entity.TenantCRUDEntity;
import io.wx.modules.mongo.exception.BadRequestException;

/**
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 */
public abstract class TenantCRUDController<T extends TenantCRUDEntity> extends CRUDController<T> {

  public abstract String getTenantID();
  
  @Override
  protected void saveEntity(T entity) throws BadRequestException {
    if (entity != null) {
      entity.setTenant(getTenantID());
    } 
    super.saveEntity(entity);
  }

  @Override
  public JsonObject getRequestParams() {
    JsonObject params = RequestUtils.getRequestParams(requestController);

    //always filter on a fixed tenant
    params.put("tenant", getTenantID());
    return params;
  }
}
