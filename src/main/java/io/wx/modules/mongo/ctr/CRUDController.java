/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo.ctr;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.wx.core3.common.WXTools;
import io.wx.core3.di.l4j.InjectLogger;
import io.wx.core3.http.RequestController;
import io.wx.core3.http.RequestMapping;
import io.wx.core3.http.URLParam;
import io.wx.core3.http.traits.BodyHandlerTrait;
import io.wx.core3.http.traits.TraitConf;
import io.wx.modules.mongo.MongoPersistor;
import io.wx.modules.mongo.entity.BaseEntity;
import io.wx.modules.mongo.entity.CRUDEntity;
import io.wx.modules.mongo.exception.BadRequestException;
import io.wx.modules.mongo.exception.EntityNotFoundException;
import io.wx.modules.mongo.handler.MongoResponseHandler;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Date;
import javax.inject.Inject;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 */
public class CRUDController<T extends CRUDEntity> {

  protected Class<?> entityClass;

  @Inject
  protected MongoPersistor mongoPersistor;

  @Inject
  protected RequestController requestController;

  @InjectLogger
  protected Logger logger;

  public CRUDController() {
    Type gs = getClass().getGenericSuperclass();
    if (gs != null && gs instanceof ParameterizedType) {
      ParameterizedType pt = (ParameterizedType) gs;
      // there is and must be only one Type
      Type t = pt.getActualTypeArguments()[0];
      this.entityClass = (Class<T>) t;
    } else {
      throw new TypeNotPresentException("cant resolve <T>", null);
    }
  }

  @RequestMapping(path = "/:id")
  public void get(@URLParam String id) {
    JsonObject query = getRequestParams();
    query.remove("id");
    query.put("_id", id);
    mongoPersistor.findOne(query, new MongoResponseHandler<T>(entityClass) {
      @Override
      public void finish() {
        if (getEntity() != null) {
          handleResponse(getEntity());
        } else {
          handleError("Error querying the entity with id " + id, getException());
        }
      }
    });
  }

  @RequestMapping
  public void listAll() {
    try {
      JsonObject query = getRequestParams();
      mongoPersistor.find(query, new MongoResponseHandler<T>(entityClass) {
        @Override
        public void finish() {
          if (getEntities() != null) {
            handleResponse(getEntities());
          } else {
            handleError("Error querying the entity", getException());
          }
        }
      });

    } catch (Exception ex) {
      handleError("cant list " + entityClass, ex);
    }
  }

  @RequestMapping(path = "/:id", method = HttpMethod.DELETE)
  public void delete(@URLParam String id) {
    JsonObject query = getRequestParams();
    query.remove("id");
    query.put("_id", id);
    try {
      mongoPersistor.findOne(query, new MongoResponseHandler<T>(entityClass) {
        @Override
        public void finish() {
          if (getEntity() != null) {
            mongoPersistor.remove(query, new MongoResponseHandler<T>(entityClass) {
              @Override
              public void finish() {
                try {
                  if (success) {
                    handleResponse("");
                  } else {
                    throw new EntityNotFoundException();
                  }
                } catch (Exception ex) {
                  handleError("cant delete entity with id " + id, ex);
                }
              }
            });
          } else {
            handleError("Error querying the entity with id " + id, getException());
          }
        }
      });
    } catch (Exception ex) {
      handleError("cant delete entity with id " + id, ex);
    }
  }

  @RequestMapping(method = HttpMethod.POST, traits = @TraitConf(trait = BodyHandlerTrait.class))
  public void create() {
    try {
      T entity = getEntityToSave();
      entity.setCreated(new Date());
      saveEntity(entity);
    } catch (BadRequestException ex) {
      handleError("cant save entity", ex);
    }
  }

  @RequestMapping(method = HttpMethod.PUT, traits = @TraitConf(trait = BodyHandlerTrait.class))
  public void update() {
    try {
      final T entity = getEntityToSave();

      //try to find the entity
      JsonObject query = getRequestParams();
      query.put("_id", entity.getId());
      mongoPersistor.findOne(query, new MongoResponseHandler<T>(entityClass) {
        @Override
        public void finish() {
          try {
            if (getEntity() != null) {
              entity.setUpdated(new Date());
              saveEntity(entity);
            } else {
              throw new EntityNotFoundException();
            }
          } catch (BadRequestException | EntityNotFoundException ex) {
            handleError("cant save entity", ex);
          }
        }
      });
    } catch (Exception ex) {
      handleError("cant save entity", ex);
    }

  }

  protected void saveEntity(T entity) throws BadRequestException {
    if (entity != null) {
      mongoPersistor.save(entity, new MongoResponseHandler<T>(entityClass) {
        @Override
        public void finish() {
          if (getEntity() != null) {
            handleResponse(getEntity());
          } else {
            handleError("Error saving the entity ", getException());
          }
        }
      });
    } else {
      throw new BadRequestException("Invalid entity");
    }
  }

  protected T getEntityToSave() throws BadRequestException {
    T entity = null;
    try {
      entity = (T) WXTools.mapper.readValue(this.requestController.getRequestBodyAsString(), entityClass);
    } catch (Exception ex) {
      throw new BadRequestException(ex.getMessage());
    }
    return entity;
  }

  public boolean entityFound(BaseEntity entity) {
    return entity != null && entity.getId() != null;
  }

  public JsonObject getRequestParams() {
    return RequestUtils.getRequestParams(requestController);
  }

  public void handleResponse(Object objectToResponse) {
    RequestUtils.handleResponse(requestController, objectToResponse);
  }

  public void handleError(String error, Exception ex) {
    RequestUtils.handleError(requestController, error, ex);
  }

}
