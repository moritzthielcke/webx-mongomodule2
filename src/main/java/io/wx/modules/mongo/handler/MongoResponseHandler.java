/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo.handler;

import io.vertx.core.json.JsonObject;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

/**
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 */
public abstract class MongoResponseHandler<T> {

  private T entity;
  private List<T> entities;
  private Class<?> entityClass;

  public MongoResponseHandler() {
    this(null);
  }

  /**
   * *
   *
   * @param entityClass the entity class, if null we try to get the class from
   * ParameterizedType T
   */
  @SuppressWarnings("UnusedAssignment")
  public MongoResponseHandler(Class<?> entityClass) {
    if (entityClass == null) {
      Type gs = getClass().getGenericSuperclass();
      if (gs != null && gs instanceof ParameterizedType) {
        ParameterizedType pt = (ParameterizedType) gs;
        // there is and must be only one Type
        Type t = pt.getActualTypeArguments()[0];
        this.entityClass = (Class<T>) t;
      } else {
        throw new TypeNotPresentException("cant resolve <T>", exception);
      }
    } else {
      this.entityClass = entityClass;
    }
  }

  public Class<?> getEntityClass() {
    return entityClass;
  }

  protected JsonObject response;
  protected Boolean success = false;
  protected Exception exception;

  public JsonObject getResponse() {
    return response;
  }

  public Boolean isSuccess() {
    return success;
  }

  public Exception getException() {
    return exception;
  }

  public T getEntity() {
    return entity;
  }

  public void setEntity(T entity) {
    this.entity = entity;
  }

  public List<T> getEntities() {
    return entities;
  }

  public void setEntities(List<T> entities) {
    this.entities = entities;
  }

  public Boolean getSuccess() {
    return success;
  }

  public void setSuccess(Boolean success) {
    this.success = success;
  }

  public abstract void finish();

  public void setException(Exception exception) {
    this.exception = exception;
  }
}
