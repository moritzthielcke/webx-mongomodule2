/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo.exception;

/**
 *
 * @author Jose Luis Conde <jose.linares@code-mitte.de>
 */
public class BadRequestException extends Exception {

    /**
     * Creates a new instance of <code>BadRequestException</code> without
     * detail message.
     */
    public BadRequestException() {
    }

    /**
     * Constructs an instance of <code>BadRequestException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public BadRequestException(String msg) {
        super(msg);
    }
}
