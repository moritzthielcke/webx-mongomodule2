package io.wx.modules.mongo.entity;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * @author moritz
 */
@Retention( RetentionPolicy.RUNTIME )
public @interface Entity{
    String collection();
}
