/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo.entity;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 */
@Retention( RetentionPolicy.RUNTIME )
public @interface RemoveReferences {
  TargetEntity[] value();
}
