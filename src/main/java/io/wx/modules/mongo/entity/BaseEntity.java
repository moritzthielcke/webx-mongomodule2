/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.vertx.core.json.JsonArray;
import java.io.Serializable;

/**
 *
 * @author moritz
 */
public class BaseEntity implements Serializable {

  @JsonProperty("_id")
  private String id;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public JsonArray describeIndexes() {
    return null;
  }

}
