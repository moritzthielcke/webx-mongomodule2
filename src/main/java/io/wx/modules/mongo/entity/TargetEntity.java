/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo.entity;

/**
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 */
public @interface TargetEntity {
    Class entity();  
    String joinColumn();  
    String updateColumn() default "";  
}
