/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo.entity;

/**
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 */
public class TenantCRUDEntity extends CRUDEntity{
    private String tenant;

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }
}
