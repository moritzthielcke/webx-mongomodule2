/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo.entity;

import java.util.Date;

/**
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 */
public class CRUDEntity extends BaseEntity {
    private Date created;
    private Date updated;

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
}
