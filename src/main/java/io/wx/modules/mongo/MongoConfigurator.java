/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo;

import com.google.inject.AbstractModule;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.wx.core3.config.AppConfig;
import io.wx.core3.config.AppConfig.Stage;
import io.wx.core3.config.Configurator;
import io.wx.core3.config.Credentials;
import io.wx.core3.config.SimpleConfig;
import io.wx.core3.http.app.AbstractApplication;
import io.wx.modules.mongo.entity.BaseEntity;
import io.wx.modules.mongo.entity.Entity;
import java.io.File;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

/**
 * defines a module called mongo -> @Inject MongoPersistor.class
 *
 * @author Jose Luis Conde Linares<jlinares@aptly.de>
 */
public class MongoConfigurator implements Configurator {

    private static final Logger logger = LogManager.getLogger(MongoConfigurator.class);
    private static final Integer MONGO_PORT = 27017;

    private final AppConfig configuration;
    private final String poolName;
    private MongoClient mongoClient;
    private MongoPersistor mongoPersistor;
    private Boolean connected = false;
    private Boolean mongoAppTerminate = true;
    private Integer mongoTimeout = 10; //seconds

    public MongoConfigurator() {
        this(MongoApp.DEFAULT_POOL_NAME);
    }

    public MongoConfigurator(String poolName) {
        this(poolName, new SimpleConfig());
    }

    public MongoConfigurator(String poolName, AppConfig configuration) {
        this.configuration = configuration;
        this.poolName = poolName;
        this.configuration.setCredentials(getCrendentials());
        String envVariable = System.getenv().get("MONGO_APP_TERMINATE");
        if (envVariable != null && envVariable.equalsIgnoreCase("false")) {
            mongoAppTerminate = false;
        }
    }

    @Override
    public AbstractApplication doConfig(AbstractApplication a) {
        Map modules = a.getModules();
        modules.put("mongo", new AbstractModule() {
            @Override
            protected void configure() {
                try {
                    mongoClient = MongoClient.createShared(a.getCore().getVertx(), getPersistorCfg(), poolName);

                    CountDownLatch latch = new CountDownLatch(1);
                    checkConnectionWithMongo(latch);
                    logger.info("Connecting with MongoDB...");
                    latch.await(mongoTimeout, TimeUnit.SECONDS);

                    if (connected) {
                        bind(MongoClient.class).toInstance(mongoClient);

                        mongoPersistor = new MongoPersistor();
                        bind(MongoPersistor.class).toInstance(mongoPersistor);

                        createIndexes(a.getCore().getReflections());
                    } else {
                        logger.error("Could not connect with mongoDB");
                        if (mongoAppTerminate) {
                            a.getCore().getVertx().close();
                            System.exit(0);
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Error during the configuration of mongoDB. " + ex.getMessage());
                    if (mongoAppTerminate) {
                        a.getCore().getVertx().close();
                        System.exit(0);
                    }
                }
            }

            private void checkConnectionWithMongo(CountDownLatch latch) {
                mongoClient.getCollections(res -> {
                    if (res.succeeded()) {
                        connected = true;
                        latch.countDown();
                    } else {
                        logger.error("Error connecting with the mongoDB." + res.cause().getMessage());
                    }
                });
            }
        });
        return a;
    }

    public JsonObject getPersistorCfg() {
        JsonObject persistorCfg = new JsonObject();
        String mongoURL = null;
        //is there a mongolab config?
        Map<String, String> credentials = configuration.getAddonCredentials().getCredentialMap("MONGOLAB");
        if (credentials != null) {
            mongoURL = credentials.get("MONGOLAB_URI");
        } //if not, mongosoup config ?
        else {
            credentials = configuration.getAddonCredentials().getCredentialMap("mongosoup");
            if (credentials == null) {
                credentials = configuration.getAddonCredentials().getCredentialMap("MONGOSOUP");
            }
            if (credentials != null) {
                mongoURL = credentials.get("MONGOSOUP_URL");
            }
        }

        /**
         * format is like: With Credentials
         * "mongodb://APP_NAME:PASSWORD@dbs001.mongosoup.de:99//DBNAME");
         *
         * Without Credentials "mongodb://dbs001.mongosoup.de:99//DBNAME");
         */
        if (mongoURL != null) {
            if (mongoURL.contains("@")) //Connect with credentials
            {
                Matcher matcher = Pattern.compile("mongodb://([^:]*):([^@]*).([^/]*)//?([^\\?]*)").matcher(mongoURL);
                if (matcher.find() && matcher.groupCount() == 4) {
                    String username = matcher.group(1);
                    String password = matcher.group(2);
                    String authSource = matcher.group(4);
                    if (!username.equals("")) {
                        persistorCfg.put("username", username);
                        persistorCfg.put("password", password);

                        /*
             * MONGODB-CR for mongo < 3.0
                         */
                        //          persistorCfg.put("authMechanism", "MONGODB-CR");
                        persistorCfg.put("authSource", authSource);
                    }

                    String[] getHost = matcher.group(3).split(":");
                    String host = getHost[0];
                    Integer port = MONGO_PORT;
                    if (getHost.length > 1) {
                        port = Integer.valueOf(getHost[1]);
                    }

                    persistorCfg.put("host", host);
                    persistorCfg.put("port", port);
                    persistorCfg.put("db_name", authSource);
                    logger.info(" MongoDB connecting with credentials > " + persistorCfg.getString("host") + ":" + persistorCfg.getInteger("port") + "/" + persistorCfg.getString("db_name"));
                } else {
                    logger.fatal("!!!! invalid MONGOSOUP_URL format !!!! ");
                }
            } else {  //Connect without credentials
                Matcher matcher = Pattern.compile("mongodb://([^/]*)//?([^\\?]*)").matcher(mongoURL);
                if (matcher.find() && matcher.groupCount() == 2) {//
                    String authSource = matcher.group(2);
                    persistorCfg.put("authSource", authSource);

                    String[] getHost = matcher.group(1).split(":");
                    String host = getHost[0];
                    Integer port = MONGO_PORT;
                    if (getHost.length > 1) {
                        port = Integer.valueOf(getHost[1]);
                    }

                    persistorCfg.put("host", host);
                    persistorCfg.put("port", port);
                    persistorCfg.put("db_name", authSource);
                    logger.info(" MongoDB connecting without credentials > " + persistorCfg.getString("host") + ":" + persistorCfg.getInteger("port") + "/" + persistorCfg.getString("db_name"));
                } else {
                    logger.fatal("!!!! invalid MONGOSOUP_URL format !!!! ");
                }
            }
        } else {
            logger.fatal("!!!!! mongodb configuration missing  !!!!");
            logger.fatal("!!!!! your database is not ready !!!!");
        }
        return persistorCfg;
    }

    /**
     * *
     * creates indexes based on the given reflections
     *
     * @param reflections
     */
    private void createIndexes(Reflections reflections) {
        createIndexes(reflections.getTypesAnnotatedWith(Entity.class));
    }

    /**
     * *
     * creates indexes for the given set of classes
     *
     * @param entitys
     */
    private void createIndexes(Set<Class<?>> entitys) {
        for (Class e : entitys) {
            try {
                //@todo how to make that more sexy?
                Object o = e.newInstance();
                JsonArray indexes = ((BaseEntity) o).describeIndexes();
                if (indexes != null) {
                    createIndexes(indexes, e);
                }
            } catch (ClassCastException cex) {
                logger.warn(e.getName() + " does not use BaseEntity.class, skipping index creation");
            } catch (Exception ex) {
                logger.error("cant create index on " + e.getName(), ex);
            }
        }
    }

    /**
     * *
     * create index for class
     *
     * @param indexes
     * @param type
     */
    private void createIndexes(JsonArray indexes, Class type) {
        JsonObject createIndexCMD = new JsonObject();
        final String collectionName = MongoPersistor.getCollectionName(type);
        createIndexCMD.put("createIndexes", collectionName);
        createIndexCMD.put("indexes", indexes);

        mongoClient.runCommand("createIndexes", createIndexCMD, res -> {
            String msg = " > MongoDB create Index on " + collectionName;
            if (res.succeeded()) {
                JsonObject result = res.result();
                logger.info(msg + " [ok] " + result.encodePrettily());
                // etc
            } else {
                logger.info(msg + " [fail] " + res.cause().getMessage());
            }
        });

    }

    /**
     * *
     * reads addon credentials
     *
     * @return
     */
    public Credentials getCrendentials() {
        if (this.configuration.getStage().equals(Stage.TEST)) {
            if (new File("./cfg/addon.creds.test").exists()) {
                return new Credentials("./cfg/addon.creds.test");
            } else if (new File("./cfg/addon.creds").exists()) {
                return new Credentials("./cfg/addon.creds");
            } else {
                return new Credentials();
            }
        } else {
            String credFile = System.getenv().get("CRED_FILE");
            if (credFile != null) { // @todo exists ?
                if (!new File(credFile).exists()) {
                    logger.info("cant find System.getenv().get(CRED_FILE) =>" + credFile);
                    return new Credentials();
                }
                return new Credentials(System.getenv().get("CRED_FILE"));
            } else if (new File("./cfg/addon.creds").exists()) {
                return new Credentials("./cfg/addon.creds");
            } else {
                return new Credentials();
            }
        }
    }

    public MongoConfigurator setMongoAppTerminate(Boolean mongoAppTerminate) {
        this.mongoAppTerminate = mongoAppTerminate;
        return this;
    }

    public MongoConfigurator setMongoTimeout(Integer mongoTimeout) {
        this.mongoTimeout = mongoTimeout;
        return this;
    }

    public Boolean getMongoAppTerminate() {
        return mongoAppTerminate;
    }

    public Integer getMongoTimeout() {
        return mongoTimeout;
    }

}
